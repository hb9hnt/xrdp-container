
# Running the container for local users

```
sudo podman run -d \
  --name xrdp_container \
  -h xrdp_container \
  -e USERS="user1 user2" \
  -e HOST_PASSWD=/etc/passwd.host \
  -e HOST_SHADOW=/etc/shadow.host \
  -v /etc/passwd:/etc/passwd.host:ro \
  -v /etc/shadow:/etc/shadow.host:ro \
  -v /home:/home:Z \
  -p 33890:3389 \
  xrdp_container

```
